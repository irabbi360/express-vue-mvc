//@ts-check
const fs = require('fs');
const ytdl = require('ytdl-core');
/**
 * Post Route Controller
 * @param {object} router
 */
module.exports = (router) => {
    router.get("/post",
        /**
         * @param {object} req
         * @param {object} res
         */
        (req, res) => {
            const data = {
                message: "POST",
            };
            req.vueOptions.head.title = "Post Example";
            req.vueOptions.head.scripts.push({
                src: "https://unpkg.com/axios/dist/axios.min.js",
            });
            res.renderVue("post/post.vue", data, req.vueOptions);
        },
    );

    router.post("/post",
        /**
         * @param {object} req
         * @param {object} res
         */
        (req, res) => {
            /*const data = {
                message: "POST",
                body: req.body,
            };*/
            const url = req.body.url;
            console.log(url)

            ytdl.getInfo(url, (err, info) => {
              if (err) throw err;
              //let format = ytdl.chooseFormat(info.formats, { quality: '134' });
              /*if (format) {
                console.log('Format found!');
              }*/
                //return console.log(info.formats);
                const data = info;
                res.json(data);
            });

            
        },
    );
};
